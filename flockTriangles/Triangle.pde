class Triangle {

   int maxSize = 25 ;
  int size = 18 ;
  
Triangle(){

}
  
  
  
  void draw(int xx,int yy, float _size , float _rot){
  
   size = int(_size);
    
    pushMatrix();
    
    translate(xx,yy);
    
    rotate(_rot);
    
    pushMatrix();
    stroke(#ff0000);
    line(0,0,size,0);
    rotate(2 *(PI/3));
    stroke(#0000ff);
    line(0,0,size,0);
    rotate(2 *(PI/3));
    stroke(#00ff00);
    line(0,0,size,0);
    popMatrix();
    popMatrix();
  }
  
}
